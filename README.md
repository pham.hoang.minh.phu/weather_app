# weather_app

## description
This is a simple weather web app. We can tracking weather.
## Features 
- Show weather of 5 days by location.
- Search city by name.
- Multiple language support.
- Multiple theme support.
# Before we start
- Install NodeJs
- Have package management (npm or yarn)
# Technologies and Tools
- React
- Jest 
- Redux & Redux Saga
- i18n
- styled-component

## Getting started
- For local you need run server first. Server will forward and skip cors for application
## Server
- Install dependence packages \
cd weather_app/backend \
npm install or yarn
- Start server \
npm start
## Front end
- Install dependence packages \
cd weather_app/frontend \
npm install
- Start server \
npm start
- See UI on browser \
access http://localhost:3000

## Deployment
## Note: We can change config about api end point and metadata url on .env.production for production environment
- Go to frontend
- Run command \
  npm run build or yarn build
- You can see production build directory 
- Deploy by web server

## Source Structure

```sh
- |-- backend --------------------------------------// Server for run local.
- |-- frontend -------------------------------------// Locate store fr.
- |----- puclic ------------------------------------// Public data for web Ext: img or static file.
- |----- src ---------------------------------------// Source code for web ap.p
- |-------- components -----------------------------// Source code and test about components use in web app. 
- |-------- config ---------------------------------// Source code for config use in web app.
- |-------- constants ------------------------------// Source code for constant use in web app.
- |-------- hooks ----------------------------------// Custom hook use in web app.
- |-------- pages ----------------------------------// store code for pages.
- |-------- layouts --------------------------------// store code for layout component Ext: Footer, Header.
- |-------- redux ----------------------------------// store action and reducer of redux.
- |-------- sagas ----------------------------------// store saga function access side effect before call redux.
- |-------- themes ---------------------------------// store fixed theme use in app.
- |-------- hoc ------------------------------------// store HOC (High Order Component) use in app.
- |-------- context --------------------------------// store context use in app.
- |-------- translate ------------------------------// store file translate use in app.
- |-------- utils ----------------------------------// store helper function use in app.
- |-------- .env.development  ----------------------// Environment config file for local and development environment 
- |-------- .env.producttion  ----------------------// Environment config file for production environment 
- |-------- App.js  --------------------------------// Main Component 
- |-------- App..store.js  -------------------------// Set up store of redux.
- |-------- i18n  ----------------------------------// For set up translate
- |-------- setupTest  -----------------------------// For set up test and mock
```





import defaultTheme from './default';
import otherTheme from './other';

const themes = {
  default : {...defaultTheme},
  other: {...otherTheme}
}

export default themes;

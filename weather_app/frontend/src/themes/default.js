const colors = {
    primary: '#dc3224',
    primaryDarker: '#c92d21',
    primaryLight: '#fbebe9',
    primaryLighter: '#fdf5f4',
  
    primaryWhite: '#ffffff',
  
    secondary: '#4b4fa6',
    secondaryDarker: '#4448a0',
    secondaryLighter: '#ededf6',
  
    tertiaryGreen: '#067e41',
    tertiaryGreenDarker: '#036835',
    tertiaryGreenLighter: '#e6f2ec',
  
    tertiaryYellow: '#D97F00',
    tertiaryYellowLighter: '#FFF4E6',
  
    tertiaryOrange: '#c94318',
    tertiaryOrangeDarker: '#b73810',
    tertiaryOrangeLighter: '#faece8',
  
    tertiaryBlue: '#2f73d2',
    tertiaryBlueDarker: '#2765bd',
    tertiaryBlueLighter: '#eaf1fb',
  
    darkBlack: '#212124',
    overlayBlack: '#0003',
    greyDarker: '#4c4c50',
    greyDark: '#75767A',
  
    grey: '#c5c9d0',
    greyLight: '#d9dbe0',
    greyLighter: '#f8f8f8',
    greyBorder: '#E5E5E5',
  
    starRatedColor: '#FFC226',
    starEmptyColor: '#C5C9D0',
  
    gray: '#DADADA',
  };
  
  const fontSizes = {
    XXSmall: '0.3rem',
    XSmall: '0.75rem',
    small: '0.875rem',
    normal: '1rem',
    medium: '1.125rem',
    titleSmall: '1.25rem',
    titleNormal: '1.5rem',
    titleMedium: '2rem',
    titleLarge: '3rem',
    titleExtraLarge: '4rem',
    titleXXLarge: '5rem',
  };
  const space = {
    none: '0px',
    mini: '2px',
    nano: '3px',
    micro: '4px',
    xtiny: '7px',
    tiny: '8px',
    XXXSmall: '10px',
    XXSmall: '11px',
    XSmall: '12px',
    small: '16px',
    medium: '18px',
    normal: '20px',
    standard: '24px',
    extraStandard: '26px',
    semi: '32px',
    semiMedium: '36px',
    semiLarge: '40px',
    large: '48px',
    XLarge: '64px',
    semiXXLarge: '80px',
    semiXXXLarge: '108px',
    XXLarge: '128px',
    XXXLarge: '180px',
  };
  
  const breakpoints = ['840px', '1440px'];
  const containerMaxWidth = 1224;
  const fontFamily = 'DM Sans';
  const shadows = {
    lightTop: '0px -2px 6px rgba(0, 0, 0, 0.04)',
  };
  
  const fontWeight = {
    normal: 400,
    medium: 500,
    bold: 500,
    large: 700,
  }
  
  const lineHeight = {
    XSmall: '14px',
    XXSmall: '16px',
    small: '18px',
    normal: '20px',
    semiLarge: '21px',
    large: '22px',
    XLarge: '24px',
    XXLarge: '28px',
    titleSmall: '30px',
    titleNormal: '36px',
    titleMedium: '48px',
    titleLarge: '72px',
  }
  const borderRadius = {
    small: '3px',
    normal: '4px',
    medium: '16px',
    large: '40px',
    XLarge: '50px',
  }
  
  
  const mainTheme = {
    breakpoints,
    space,
    fontSizes,
    colors,
    containerMaxWidth,
    fontFamily,
    shadows,
    fontWeight,
    lineHeight,
    borderRadius
  }
  
  export default mainTheme;
  
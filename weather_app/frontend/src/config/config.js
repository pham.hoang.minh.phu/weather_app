const Config = {
    REACT_APP_BASE_URL: process.env.REACT_APP_BASE_URL,
    META_WEATHER_URL: process.env.REACT_APP_META_WEATHER_URL,
    ENV: process.env.REACT_APP_ENVIRONMENT,
}

export default Config;
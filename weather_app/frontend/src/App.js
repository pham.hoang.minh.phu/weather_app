import React, { Suspense } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import styled from 'styled-components';
import { Provider } from 'react-redux';
import { store } from './App.store'
import { ROUTE } from './constants/router';
import { Home } from './pages/Home';

import ThemeHandler from './hoc/theme';

const MainStyle = styled.div`  
  position: 'relative';
  minHeight: '100%';
  height: '100%';
`;

const Main = () => {
  return (
      <Provider store={store}>
        <Router>
          <MainStyle data-testid="test-main-app">
            <Suspense>
              <Switch>
                <Route path={ROUTE.ROOT}>
                  <Home></Home>
                </Route>
              </Switch>
            </Suspense>
          </MainStyle>
        </Router>
      </Provider>
  )
}

function App() {
  return (
    <ThemeHandler>
      <Main />
    </ThemeHandler>
  );
}


export default App;

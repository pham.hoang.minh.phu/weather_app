import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import createRootSaga from './sagas';
import rootReducer from './redux/rootReducer';
import { actionCreators as cityActions } from './redux/city/city.meta';
import { actionCreators as weatherActions } from './redux/weather/weather.meta';

const sagaMiddleware = createSagaMiddleware();
const getComposer = () => {
  const reduxDevToolsCompose =
    (process.env.NODE_ENV === 'development' &&
      process.browser &&
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || // eslint-disable-line no-underscore-dangle
    compose;
  return reduxDevToolsCompose;
};

const middleware = [sagaMiddleware];


if (process.env.NODE_ENV === 'development') {
  middleware.push(createLogger({ collapsed: true }));
}

const rootSaga = createRootSaga();
const middlewareEnhancer = applyMiddleware(...middleware);
const composer = getComposer(); 
const enhancer = composer(middlewareEnhancer);

const store = createStore(rootReducer, {}, enhancer);
sagaMiddleware.run(rootSaga);

const actions = { 
  cityActions,
  weatherActions,
}

export { store, actions };

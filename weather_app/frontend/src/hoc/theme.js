import React, { useState } from 'react';
import ThemeHandleContext from '../context/theme';
import { DEFAULT_THEME } from '../constants/constants';
import { ThemeProvider } from 'styled-components';
import themes from '../themes/theme';
const ThemeHandler = ({ children }) => {
    const [themeName, setThemeName] = useState(DEFAULT_THEME);
    return <ThemeHandleContext.Provider value={{
        themeName, setThemeName
    }}>
        <ThemeProvider theme={themes[`${themeName}`]}>
            {children}
        </ThemeProvider>

    </ThemeHandleContext.Provider>
}

export default ThemeHandler

import { types } from './city.meta';
import { handleActions } from 'redux-actions';
import { LOCATION_TYPE } from '../../constants/constants';

const initialState = {
    cityList: [],
    isFetching: false,
    isFailed: false
};

export function fetchCityList(state) {
    return {
        ...state,
        isFetching: true,
    };
}

export function fetchCityListCompleted(state, action) {
    const { payload: data } = action;

    const cityList = data.map(city => {
        const {
            title: cityName,
            location_type: locationType,
            woeid: locationId,
        } = city;
        // just apply for city 
        if (locationType === LOCATION_TYPE.CITY) {
            return {
                locationId: locationId,
                name: cityName,
                locationType: locationType,
            }
        }

        return null;
    });

    return {
        ...state,
        isFetching: false,
        isFailed: false,
        cityList: cityList.filter(i => i)
    }
}

export function fetchCityListFailed(state) {
    return {
        ...state,
        isFailed: true,
        isFetching: false,
    };
}

export default handleActions(
    {
        [types.FETCH_CITY_LIST_COMPLETED]: fetchCityListCompleted,
        [types.FETCH_CITY_LIST]: fetchCityList,
        [types.FETCH_CITY_LIST_FAILED]: fetchCityListFailed,
    },
    initialState
);

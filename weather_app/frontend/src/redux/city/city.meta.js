import { createAction } from 'redux-actions'; 

const cityActions = {
    FETCH_CITY_LIST: 'FETCH_CITY_LIST',
    FETCH_CITY_LIST_COMPLETED: 'FETCH_CITY_LIST_COMPLETED',
    FETCH_CITY_LIST_FAILED: 'FETCH_CITY_LIST_FAILED',
};

const actionCreators = {
    fetchCityList: createAction(cityActions.FETCH_CITY_LIST),
    fetchCityListCompleted: createAction(cityActions.FETCH_CITY_LIST_COMPLETED),
    fetchCityListFailed: createAction(cityActions.FETCH_CITY_LIST_FAILED)
};
  
export { cityActions as types, actionCreators };
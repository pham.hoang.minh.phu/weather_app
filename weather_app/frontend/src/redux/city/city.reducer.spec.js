import cityReducer, {
    fetchCityList,
    fetchCityListFailed,
    fetchCityListCompleted
} from './city.reducer';

describe("City reducer test", () => {
    it('should export a function', () => {
        expect(typeof cityReducer).toEqual('function');
    });

    const sampleAction = {
        payload: [
            {
                title: "A",
                location_type: "City",
                woeid: "1",
            },
            {
                title: "B",
                location_type: "City",
                woeid: "2",
            },
            {
                title: "C",
                location_type: "Country",
                woeid: "3",
            }
        ]        
    }

    const initialState = {
        cityList: [],
        isFetching: false,
        isFailed: false
    };

    const mockState = {
        isFetching: false,
        isFailed: false,
        cityList: [
            {
                locationId: "1",
                name: "A",
                locationType: "City",
            },
            {
                locationId: "2",
                name: "B",
                locationType: "City",
            }
        ],
    }

    it('should update the isFetching status on fetchCityList', () => {
        const state = fetchCityList(initialState, sampleAction);
        expect(state.isFetching).toBe(true);
    });

    it('should store the city list correctly', () => {
        const state = fetchCityListCompleted(initialState, sampleAction);
        expect(state).toEqual(mockState);
    });

    it('should update the isFetching status on fetchCityListFailed', () => {
        const state = fetchCityListFailed(initialState, sampleAction);
        expect(state.isFetching).toBe(false);
        expect(state.isFailed).toBe(true);
      });
    
})
import { combineReducers } from 'redux';
import city from './city/city.reducer';
import weather from './weather/weather.reducer';

export default combineReducers({
    city,
    weather
});


import { types } from './weather.meta';
import { handleActions } from 'redux-actions';

const initialState = {
    location: null,
    weatherList: [],
    isFetching: false,
    isFailed: false
};

export function fetchWeatherByLocation(state) {
    return {
        ...state,
        isFetching: true,
    };
}

export function fetchWeatherByLocationCompleted(state, action) {
    const { payload: data } = action;
    const {
        consolidated_weather: consolidateWeather,
        time,
        timezone,
        title,
        woeid: locationId,
        location_type: locationType,
    } = data || {}
    const weatherList = consolidateWeather?.map((item, index) => {
        const {
            id: weatherId,
            weather_state_abbr: weatherStateAbbr,
            applicable_date: applicableDate,
            min_temp: minTemp,
            max_temp: maxTemp,
            the_temp: avgTemp,
            wind_speed: windSpeed,
            air_pressure: airPressure,
            humidity,
            predictability,
        } = item;

        // We would like get 5 days
        if (index < 5)
            return {
                weatherId,
                weatherShortKey: weatherStateAbbr,
                applicableDate,
                minTemp,
                maxTemp,
                avgTemp,
                windSpeed,
                airPressure,
                humidity,
                predictability,
            }

        return null;
    });


    

    return {
        ...state,
        isFetching: false,
        isFailed: false,
        location: {
            locationId: locationId || "",
            time: time,
            timezone: timezone,
            locationType,
            locationName: title || "",
        },
        weatherList: weatherList.filter(i => i)
    }
}

export function fetchWeatherByLocationFailed(state) {
    return {
        ...state,
        isFailed: true,
        isFetching: false,
    };
}

export default handleActions(
    {
        [types.FETCH_WEATHER_BY_LOCATION]: fetchWeatherByLocation,
        [types.FETCH_WEATHER_BY_LOCATION_COMPLETED]: fetchWeatherByLocationCompleted,
        [types.FETCH_WEATHER_BY_LOCATION_FAILED]: fetchWeatherByLocationFailed,
    },
    initialState
);

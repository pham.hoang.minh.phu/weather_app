import weatherReducer, {
    fetchWeatherByLocation,
    fetchWeatherByLocationFailed,
    fetchWeatherByLocationCompleted
} from './weather.reducer';

describe("weather reducer test", () => {
    it('should export a function', () => {
        expect(typeof weatherReducer).toEqual('function');
    });

    const sampleAction = {
        payload: {
            consolidated_weather: [
                {
                    id: 6029389318848512,
                    weather_state_abbr: "lr",
                    applicable_date: "2021-10-18",
                    min_temp: 10.69,
                    max_temp: 16.335,
                    the_temp: 16.064999999999998,
                    wind_speed: 6.933720773199184,
                    air_pressure: 1016,
                    humidity: 81,
                    predictability: 75,
                },
                {
                    id: 1,
                    weather_state_abbr: "lr",
                    applicable_date: "2021-10-18",
                    min_temp: 10.69,
                    max_temp: 16.335,
                    the_temp: 16.064999999999998,
                    wind_speed: 6.933720773199184,
                    air_pressure: 1016,
                    humidity: 81,
                    predictability: 75,
                },
                {
                    id: 2,
                    weather_state_abbr: "lr",
                    applicable_date: "2021-10-18",
                    min_temp: 10.69,
                    max_temp: 16.335,
                    the_temp: 16.064999999999998,
                    wind_speed: 6.933720773199184,
                    air_pressure: 1016,
                    humidity: 81,
                    predictability: 75,
                },
                {
                    id: 3,
                    weather_state_abbr: "lr",
                    applicable_date: "2021-10-18",
                    min_temp: 10.69,
                    max_temp: 16.335,
                    the_temp: 16.064999999999998,
                    wind_speed: 6.933720773199184,
                    air_pressure: 1016,
                    humidity: 81,
                    predictability: 75,
                },
                {
                    id: 4,
                    weather_state_abbr: "lr",
                    applicable_date: "2021-10-18",
                    min_temp: 10.69,
                    max_temp: 16.335,
                    the_temp: 16.064999999999998,
                    wind_speed: 6.933720773199184,
                    air_pressure: 1016,
                    humidity: 81,
                    predictability: 75,
                },
                {
                    id: 5,
                    weather_state_abbr: "lr",
                    applicable_date: "2021-10-18",
                    min_temp: 10.69,
                    max_temp: 16.335,
                    the_temp: 16.064999999999998,
                    wind_speed: 6.933720773199184,
                    air_pressure: 1016,
                    humidity: 81,
                    predictability: 75,
                }


            ],
            time : "",
            timezone : "timeZone",
            title : "London",
            woeid: "1",
            location_type: "City",
        }     
    }

    const initialState = {
        location: null,
        weatherList: [],
        isFetching: false,
        isFailed: false
    };

    const mockState = {
        location: {
            locationId: "1",
            time: "",
            timezone: "timeZone",
            locationType : "City",
            locationName: "London",
        },
        weatherList: [
            {
                weatherId : 6029389318848512,
                weatherShortKey: "lr",
                applicableDate: "2021-10-18",
                minTemp: 10.69,
                maxTemp: 16.335,
                avgTemp: 16.064999999999998,
                windSpeed: 6.933720773199184,
                airPressure: 1016,
                humidity: 81,
                predictability: 75,
            },
            {
                weatherId : 1,
                weatherShortKey: "lr",
                applicableDate: "2021-10-18",
                minTemp: 10.69,
                maxTemp: 16.335,
                avgTemp: 16.064999999999998,
                windSpeed: 6.933720773199184,
                airPressure: 1016,
                humidity: 81,
                predictability: 75,
            },
            {
                weatherId : 2,
                weatherShortKey: "lr",
                applicableDate: "2021-10-18",
                minTemp: 10.69,
                maxTemp: 16.335,
                avgTemp: 16.064999999999998,
                windSpeed: 6.933720773199184,
                airPressure: 1016,
                humidity: 81,
                predictability: 75,
            },
            {
                weatherId : 3,
                weatherShortKey: "lr",
                applicableDate: "2021-10-18",
                minTemp: 10.69,
                maxTemp: 16.335,
                avgTemp: 16.064999999999998,
                windSpeed: 6.933720773199184,
                airPressure: 1016,
                humidity: 81,
                predictability: 75,
            },
            {
                weatherId : 4,
                weatherShortKey: "lr",
                applicableDate: "2021-10-18",
                minTemp: 10.69,
                maxTemp: 16.335,
                avgTemp: 16.064999999999998,
                windSpeed: 6.933720773199184,
                airPressure: 1016,
                humidity: 81,
                predictability: 75,
            }
        ],
        isFetching: false,
        isFailed: false
    }

    it('should update the isFetching status on fetchWeatherByLocation', () => {
        const state = fetchWeatherByLocation(initialState, sampleAction);
        expect(state.isFetching).toBe(true);
    });

    it('should store the weather data correctly', () => {
        const state = fetchWeatherByLocationCompleted(initialState, sampleAction);
        expect(state).toEqual(mockState);
    });

    it('should update the isFetching status on fetchWeatherByLocationFailed', () => {
        const state = fetchWeatherByLocationFailed(initialState, sampleAction);
        expect(state.isFetching).toBe(false);
        expect(state.isFailed).toBe(true);
      });
    
})
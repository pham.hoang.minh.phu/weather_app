import { createAction } from 'redux-actions'; 

const weatherActions = {
    FETCH_WEATHER_BY_LOCATION : 'FETCH_WEATHER_BY_LOCATION',
    FETCH_WEATHER_BY_LOCATION_COMPLETED: 'FETCH_WEATHER_BY_LOCATION_COMPLETED',
    FETCH_WEATHER_BY_LOCATION_FAILED: 'FETCH_WEATHER_BY_LOCATION_FAILED',
};

const actionCreators = {
    fetchWeatherByLocation: createAction(weatherActions.FETCH_WEATHER_BY_LOCATION),
    fetchWeatherByLocationCompleted: createAction(weatherActions.FETCH_WEATHER_BY_LOCATION_COMPLETED),
    fetchWeatherByLocationFailed: createAction(weatherActions.FETCH_WEATHER_BY_LOCATION_FAILED)
};
  
export { weatherActions as types, actionCreators };
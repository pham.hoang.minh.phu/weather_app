import moment from 'moment';
import { DAY_OF_WEEK } from '../constants/constants'

export const getDateOfWeek = (strDate, format)=>{
    const d = moment(strDate,format);
    return DAY_OF_WEEK[d.weekday().toString()]
}
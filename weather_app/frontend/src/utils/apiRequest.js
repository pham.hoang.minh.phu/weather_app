import axios from 'axios';

export const apiRequest = ({
    method = "GET",
    url,
    headers,
    params,
    data
}) => {
    return axios({
        method: method,
        url: url,
        params: params,
        data: data,
        headers: {
            // We can handle token in here.
            ...headers,
        },
    }).then(
        (response) => {
            const { status, data } = response;
            return {
                data: data,
                status: status,
                success: true,
            };
        },
        (error) => {
            if (error.response) {
                const { status, data } = error.response || {};
                return {
                    data: data,
                    status: status,
                    success: false,
                };
            }

            return {
                data: null,
                status: null,
                success: false,
            };


        }
    );
}
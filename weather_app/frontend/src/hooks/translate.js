import { useTranslation } from 'react-i18next';

const useTranslate = () => {
    const { t, i18n } = useTranslation();
    const translate = (key,params = []) => {
        return t(key);
    }

    const changeLanguage = (lng) => {
        i18n.changeLanguage(lng);
    };

    return { translate, changeLanguage, ...i18n }
}

export default useTranslate;
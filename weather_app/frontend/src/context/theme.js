import { createContext } from 'react';
import { DEFAULT_THEME } from '../constants/constants';

const ThemeHandleContext = createContext({
    themeName: DEFAULT_THEME,
    setThemeName: (fn)=> fn,
})

export default ThemeHandleContext;
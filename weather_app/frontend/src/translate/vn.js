const translate = {
    translation: {
        theme: {
            default : "Mặc định",
            other: "Khác"
        },
        vn: "Việt Nam",
        textBoxCity: {
            label: "Tìm kiếm thành phố",
            placeholder: "Nhập vài kí tự"
        },
        dayOfWeek: {
            MON: "Thứ hai",
            TUE: "Thứ ba",
            WED: "Thứ tư",
            THU: "Thứ năm",
            FRI: "Thứ sáu",
            SAT: "Thứ bảy",
            SUN: "Chủ nhật",
        },
        weather: {
            hc: "Trời nhiều mây",
            sn: "Tuyết",
            sl: "Mưa đá",
            h: "Mưa đá",
            t: "Dông, sấm",
            hr: "Mưa nặng hạt",
            lr: "Mưa nhỏ",
            s: "Mưa nhỏ và có nắng",
            lc: "Trời ít mây",
            c: "Trong xanh"
        }
    }
}

export default translate;
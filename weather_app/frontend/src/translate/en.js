 const translate = {
    translation: {
        theme: {
            default : "Default",
            other: "Other"
        },
        en: "English",
        textBoxCity: {
            label: "City Search",
            placeholder: "Input some keys"
        },
        dayOfWeek: {
            MON: "Monday",
            TUE: "Tuesday",
            WED: "Wednesday",
            THU: "Thursday",
            FRI: "Friday",
            SAT: "Saturday",
            SUN: "Sunday",
        },
        weather: {
            hc: "Heavy Cloud",
            sn: "Snow",
            sl: "Sleet",
            h: "Hail",
            t: "Thunderstorm",
            hr: "Heavy Rain",
            lr: "Light Rain",
            s: "Showers",
            lc: "Light Cloud",
            c: "Clear"
        }
    }
};

export default translate;
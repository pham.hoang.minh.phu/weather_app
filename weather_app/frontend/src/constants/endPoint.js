export const API_END_POINT = {
    SEARCH_CITY : "/api/location/search",
    WEATHER_BY_LOCATION: "/api/location/{locationId}"
}
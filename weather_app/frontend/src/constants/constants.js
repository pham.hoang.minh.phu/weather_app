export const DAY_OF_WEEK = {
    "0" : "SUN",
    "1" : "MON",
    "2" : "TUE",
    "3" : "WED",
    "4" : "THU",
    "5" : "FRI",
    "6" : "SAT",
} 

export const LOCATION_TYPE = {
    CITY: "City",
}

export const WEATHER_KEY = {
    HEAVY_RAIN: "hr",
    SNOW: "sn",
    SLEET: "sl",
    HAIL: "h",
    THUNDERSTORM : "t",
    LIGHT_RAIN : "lr",
    SHOWERS: "s",
    HEAVY_CLOUD :"hc",
    LIGHT_CLOUD: "lc",
    CLEAR: "c"
}

export const THEMES = ['default','other'];

export const DEFAULT_THEME = "default";
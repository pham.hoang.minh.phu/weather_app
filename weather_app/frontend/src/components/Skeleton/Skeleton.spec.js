import React from 'react';
import { render, screen, fireEvent, waitFor, logRoles } from '@testing-library/react';
import { WeatherCardSkeleton } from './Skeleton';
import ThemeHandler from '../../hoc/theme';

const data = {}
it('Render skeleton', async () => {
    render(
        <ThemeHandler>
            <WeatherCardSkeleton/>
        </ThemeHandler>
    );

    // expect(screen.getby("dayOfWeek.MON")).toBeInTheDocument();
    expect(screen.getByTestId("skeleton-weather-wrap")).toBeInTheDocument();
});

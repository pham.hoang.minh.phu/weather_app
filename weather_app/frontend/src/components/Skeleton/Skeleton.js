import * as React from 'react';
import Skeleton from '@mui/material/Skeleton';
import styled from 'styled-components';
import CardContent from '@mui/material/CardContent';


const DayOfWeekText = styled(Skeleton)`
    font-size:  ${({ theme }) => theme.fontSizes.titleMedium};
    font-weight: ${({ theme }) => theme.fontWeight.bold};
    height:10px;
    color: ${({ theme }) => theme.colors.greyDarker}
    flex: 1;
    min-width: 100px;
`

const WrapperDayOfWeek = styled.div`
    padding: 10px 20px;
    flex: 1;
    justify-content: center;
    display: flex;
`

const TemperatureRange = styled(Skeleton)`
    font-size:  ${({ theme }) => theme.fontSizes.normal};
    font-weight: ${({ theme }) => theme.fontWeight.bold};
    flex: 1;
`

const WeatherText = styled(Skeleton)`
    font-size:  ${({ theme }) => theme.fontSizes.titleSmall};
    font-weight: ${({ theme }) => theme.fontWeight.bold};
    flex: 1;
`

const Wrapper = styled.div`
    padding: 10px 20px;
    flex: 1;
    justify-content: center;
    display: flex;
`

const WeatherImage = styled(Skeleton)`
`

const WeatherCardStyle = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    border-radius: 5px;
    border: 1px solid ${({ theme }) => theme.colors.grey};
    background-color: ${({ theme }) => theme.colors.primaryWhite};
`

export const WeatherCardSkeleton = () => {
    return (
        <WeatherCardStyle
            variant="outlined"
            backgroundColor="black"
            data-testid="skeleton-weather-wrap"
        >
            <WrapperDayOfWeek>
                <DayOfWeekText  variant="text"></DayOfWeekText >
            </WrapperDayOfWeek>
            <Wrapper>
                <WeatherImage
                    width={150}
                    height={150}
                    variant="rectangular"
                />
            </Wrapper>
            <CardContent>
                <Wrapper>
                    <TemperatureRange variant="text"></TemperatureRange>
                </Wrapper>
                <Wrapper>
                    <WeatherText variant="text"></WeatherText>
                </Wrapper>
            </CardContent>
        </WeatherCardStyle>
    );
}

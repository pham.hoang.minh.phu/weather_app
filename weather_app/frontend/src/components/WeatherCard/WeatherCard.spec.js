import React from 'react';
import { render, screen, fireEvent, waitFor, logRoles } from '@testing-library/react';
import WeatherCard from './WeatherCard';
import ThemeHandler from '../../hoc/theme'

const data = {
    date: "2012-12-24",
    minTemp: 5,
    maxTemp: 10,
    weatherImg: "/test.svg",
    weatherShortKey: "hc"
}
it('Show weather card with info', async () => {
    render(
        <ThemeHandler>
            <WeatherCard
                date={data.date}
                minTemp={data.minTemp}
                maxTemp={data.maxTemp}
                weatherImg={data.weatherImg}
                weatherShortKey={data.weatherShortKey}
            />
        </ThemeHandler>
    );

    const img = screen.getByRole("img");
    expect(img).toBeInTheDocument();
    expect(screen.getByText("dayOfWeek.MON")).toBeInTheDocument();
    expect(screen.getByText("5 - 10 °C")).toBeInTheDocument();
    expect(screen.getByText("weather.hc")).toBeInTheDocument();
});

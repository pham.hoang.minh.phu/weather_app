import React from 'react'
import CardContent from '@mui/material/CardContent';
import { WeatherCardPropsType, WeatherCardDefaultProps } from './WeatherCart.propTypes';
import styled from 'styled-components';
import { getDateOfWeek } from '../../utils/date';
import { temperatureFormat } from '../../utils/number';
import { useTranslation } from 'react-i18next';

const DayOfWeekText = styled.h2`
    font-size:  ${({ theme }) => theme.fontSizes.titleMedium};
    font-weight: ${({ theme }) => theme.fontWeight.bold};
    color: ${({ theme }) => theme.colors.greyDarker}
`

const WrapperDayOfWeek = styled.div`
    padding: 10px 20px;
    flex: 1;
    justify-content: center;
    display: flex;
`

const TemperatureRange = styled.span`
    font-size:  ${({ theme }) => theme.fontSizes.normal};
    font-weight: ${({ theme }) => theme.fontWeight.bold};
`

const WeatherText = styled.span`
    font-size:  ${({ theme }) => theme.fontSizes.titleSmall};
    font-weight: ${({ theme }) => theme.fontWeight.bold};
`

const Wrapper = styled.div`
    padding: 10px 20px;
    flex: 1;
    justify-content: center;
    display: flex;
`

const WeatherImage = styled.img`
`

const WeatherCardStyle = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    border-radius: 5px;
    border: 1px solid ${({ theme }) => theme.colors.grey};
    background-color: ${({ theme }) => theme.colors.primaryWhite};
`

const WeatherCard = ({
    date,
    minTemp,
    maxTemp,
    weatherImg,
    weatherShortKey
}) => {
    const  dow  = getDateOfWeek(date,"YYYY-MM-DD");
    const { t } = useTranslation();
    
    return (
        <WeatherCardStyle
            variant="outlined"
            backgroundColor="black"
        >
            <WrapperDayOfWeek>
                <DayOfWeekText> {t(`dayOfWeek.${dow}`)}</DayOfWeekText >
            </WrapperDayOfWeek>
            <Wrapper>
                <WeatherImage
                    width={150}
                    height={150}
                    src={weatherImg}
                />
            </Wrapper>
            <CardContent>
                <Wrapper>
                    <TemperatureRange>
                        {`${temperatureFormat(minTemp)} - ${temperatureFormat(maxTemp)}  °C`}
                    </TemperatureRange>
                </Wrapper>
                <Wrapper>
                    <WeatherText>
                        {t(`weather.${weatherShortKey}`)}
                    </WeatherText>
                </Wrapper>
            </CardContent>
        </WeatherCardStyle>
    );
}

WeatherCard.propTypes = WeatherCardPropsType;

WeatherCard.defaultProps = WeatherCardDefaultProps;

export default WeatherCard;
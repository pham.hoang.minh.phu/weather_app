import { string, number } from 'prop-types';

export const WeatherCardPropsType = {
    date: string,
    minTemp: number,
    maxTemp: number,
}

export const WeatherCardDefaultProps = {
    date: '',
    minTemp: 0,
    maxTemp: 0,
}
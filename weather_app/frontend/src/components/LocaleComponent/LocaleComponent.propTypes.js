import { string, array, func } from 'prop-types';

export const LocaleComponentPropTypes = {
    language: string,
    languages: array,
    changeLanguage: func,
}

export const LocaleComponentDefaultProps = {
    language: "",
    languages: [],
    changeLanguage: (v)=>{},
}
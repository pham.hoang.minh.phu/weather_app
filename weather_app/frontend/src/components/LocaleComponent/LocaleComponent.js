import React, { useState } from 'react';
import useTranslate from '../../hooks/translate';
import Paper from '@mui/material/Paper';
import MenuList from '@mui/material/MenuList';
import MenuItem from '@mui/material/MenuItem';
import ListItemText from '@mui/material/ListItemText';
import styled from 'styled-components';
import TranslateIcon from '@mui/icons-material/Translate';
import IconButton from '@mui/material/IconButton';
import { LocaleComponentDefaultProps, LocaleComponentPropTypes } from './LocaleComponent.propTypes';
const Container = styled.div`
    flex: 1;
    display: relative;
`

const WrapperDropDownContent = styled.div`
    right: 0;
    z-index: 10;
    position: absolute;
    margin-top: 8px;
`

const PaperStyle = styled(Paper)`

`

export const LocaleComponent = ({
    language,
    languages,
    changeLanguage,
}) => {
    const { translate } = useTranslate();
    const [showPopup, setShowPopup] = useState(false)

    const handleChange = (lang) => {
        if (lang !== language) {
            changeLanguage(lang);
        }
        setShowPopup(false);
    };

    const handleClick = () => {
        setShowPopup(true);
    }

    return (
        <Container>
            <IconButton variant="outlined" onClick={handleClick} >
                <TranslateIcon></TranslateIcon>
            </IconButton>
            {
                showPopup && languages && <WrapperDropDownContent>
                    <PaperStyle variant="outlined">
                        <MenuList >
                            {
                                languages.map(i => {
                                    return (
                                        <MenuItem
                                            selected={i === language}
                                            onClick={() => {
                                                handleChange(i)
                                            }}
                                        >
                                            <ListItemText >{translate(i)}</ListItemText>
                                        </MenuItem>)
                                })
                            }
                        </MenuList>
                    </PaperStyle>
                </WrapperDropDownContent>
            }

        </Container>
    )
}


LocaleComponent.propsType = LocaleComponentPropTypes;

LocaleComponent.defaultProps = LocaleComponentDefaultProps;

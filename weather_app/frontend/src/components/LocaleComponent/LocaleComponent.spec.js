import React from 'react';
import { render, screen, fireEvent, waitFor, logRoles } from '@testing-library/react';
import { LocaleComponent } from './LocaleComponent';
import ThemeHandler from '../../hoc/theme'


describe('Render LocaleComponent',() => {
    const changeLanguage = jest.fn();
    render(
        <ThemeHandler>
            <LocaleComponent
                language={"en"}
                languages={["en","vn"]}
                changeLanguage={changeLanguage}
            />
        </ThemeHandler>
    );
    const input = screen.getByRole("button");
    expect(input).toBeInTheDocument();
    it("Show list language and select", () => {
        fireEvent(input, new MouseEvent("click", {
            bubbles: true,
            cancelable: true,
        }))
        const item = screen.getByText("vn");
        expect(item).toBeInTheDocument();
        fireEvent(item, new MouseEvent("click", {
            bubbles: true,
            cancelable: true,
        }))
        expect(changeLanguage).toBeCalledTimes(1)
    })
})

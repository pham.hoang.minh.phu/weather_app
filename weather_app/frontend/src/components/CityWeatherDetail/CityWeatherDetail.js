import React from 'react'
import CardContent from '@mui/material/CardContent';
import { CityWeatherDetailPropsType, CityWeatherDetailDefaultProps } from './CityWeatherDetail.propTypes';
import styled from 'styled-components';
import { getDateOfWeek } from '../../utils/date';
import { temperatureFormat } from '../../utils/number';
import { useTranslation } from 'react-i18next';

const DayOfWeekText = styled.h2`
    font-size:  ${({ theme }) => theme.fontSizes.titleMedium};
    font-weight: ${({ theme }) => theme.fontWeight.bold};
    color: ${({ theme }) => theme.colors.greyDarker}
`

const WrapperDayOfWeek = styled.div`
    padding: 10px 20px;
    flex: 1;
    justify-content: center;
    display: flex;
`

const TemperatureRange = styled.span`
    font-size:  ${({ theme }) => theme.fontSizes.normal};
    font-weight: ${({ theme }) => theme.fontWeight.bold};
`

const WeatherText = styled.span`
    font-size:  ${({ theme }) => theme.fontSizes.titleSmall};
    font-weight: ${({ theme }) => theme.fontWeight.bold};
`

const Wrapper = styled.div`
    padding: 10px 20px;
    flex: 1;
    justify-content: center;
    display: flex;
`

const WeatherImage = styled.img`
    width:150px;
    height:150px;
`

const CityWeatherDetailStyle = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    border-radius: 5px;
    border: 1px solid ${({ theme }) => theme.colors.grey};
    background-color: ${({ theme }) => theme.colors.primaryWhite};
`

const CityWeatherDetail = ({
    date,
    minTemp,
    maxTemp,
    weatherImg,
    weatherShortKey
}) => {
    const  dow  = getDateOfWeek(date,"YYYY-MM-DD");
    const { t, i18n } = useTranslation();
    return (
        <CityWeatherDetailStyle
            variant="outlined"
            backgroundColor="black"
        >
            <WrapperDayOfWeek>
                <DayOfWeekText> {t(`dayOfWeek.${dow}`)}</DayOfWeekText >
            </WrapperDayOfWeek>
            <Wrapper>
                <WeatherImage
                    src={weatherImg}
                />
            </Wrapper>
          
            <CardContent>
                <Wrapper>
                    <TemperatureRange>
                        {`${temperatureFormat(minTemp)} - ${temperatureFormat(maxTemp)}  °C`}
                    </TemperatureRange>
                </Wrapper>
                <Wrapper>
                    <WeatherText>
                        {t(`weather.${weatherShortKey}`)}
                    </WeatherText>
                </Wrapper>
            </CardContent>
        </CityWeatherDetailStyle>
    );
}

export default CityWeatherDetail;

CityWeatherDetail.propsType = CityWeatherDetailPropsType;

CityWeatherDetail.defaultProps = CityWeatherDetailDefaultProps;

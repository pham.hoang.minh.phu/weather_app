import { string, number } from 'prop-types';

export const CityWeatherDetailPropsType = {
    date : string,
    minTemp: number,
    maxTemp: number,
    weatherImg: string,
    weatherShortKey: string,
}

export const CityWeatherDetailDefaultProps = {
    date : "",
    minTemp: 0,
    maxTemp: "",
    weatherImg: "",
    weatherShortKey: "",
}
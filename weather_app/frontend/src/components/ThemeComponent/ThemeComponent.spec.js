import React from 'react';
import { render, screen, fireEvent, waitFor, logRoles } from '@testing-library/react';
import { ThemeComponent }  from './ThemeComponent';
import ThemeHandler from '../../hoc/theme'
import { DEFAULT_THEME } from '../../constants/constants'


describe('Render ThemeComponent',()=>{
    const changeTheme = jest.fn();
    render(
        <ThemeHandler>
            <ThemeComponent 
                themeName={DEFAULT_THEME}
                changeTheme={changeTheme}
            />
        </ThemeHandler>
    );

    const input = screen.getByRole("button");
    expect(input).toBeInTheDocument();

    it('click button and select theme', async()=>{
        fireEvent(input, new MouseEvent('click',{
            bubbles: true,
            cancelable: true,
        }))
        const item = screen.getByText("theme.other")
        expect(item).toBeInTheDocument();
        fireEvent(item, new MouseEvent('click',{
            bubbles: true,
            cancelable: true,
        }))
        expect(changeTheme).toBeCalledTimes(1);
    })
})

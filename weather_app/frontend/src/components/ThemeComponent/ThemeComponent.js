import React, { useState } from 'react';
import Paper from '@mui/material/Paper';
import MenuList from '@mui/material/MenuList';
import MenuItem from '@mui/material/MenuItem';
import ListItemText from '@mui/material/ListItemText';
import styled from 'styled-components';
import SettingsIcon from '@mui/icons-material/Settings';
import IconButton from '@mui/material/IconButton';
import useTranslate from '../../hooks/translate';
import { THEMES } from '../../constants/constants';
import { ThemeComponentPropTypes, ThemeComponentDefaultProps } from './ThemeComponent.propTypes'; 

const Container = styled.div`
    flex: 1;
    display: relative;
`

const WrapperDropDownContent = styled.div`
    right: 0;
    z-index: 10;
    position: absolute;
    margin-top: 8px;
`

const PaperStyle = styled(Paper)`
 
`

export const ThemeComponent = ({
    themeName,
    changeTheme
}) => {
    const { translate } = useTranslate();
    const [showPopup, setShowPopup] = useState(false)

    const handleChange = (value) => {
        if (value !== themeName){
            changeTheme(value)
        }
        setShowPopup(false);
    };

    const handleClick = () => {
        setShowPopup(true);
    }

    return (
        <Container>
            <IconButton variant="outlined" onClick={handleClick} >
                <SettingsIcon></SettingsIcon>
            </IconButton>
            {
                showPopup && <WrapperDropDownContent>
                    <PaperStyle variant="outlined">
                        <MenuList >
                            {
                                THEMES.map(i => {
                                    return (
                                        <MenuItem
                                            selected={i === themeName}
                                            onClick={() => {
                                                handleChange(i)
                                            }}
                                        >
                                            <ListItemText >{translate(`theme.${i}`)}</ListItemText>
                                        </MenuItem>)
                                })
                            }
                        </MenuList>
                    </PaperStyle>
                </WrapperDropDownContent>
            }

        </Container>
    )
}


ThemeComponent.propsType = ThemeComponentPropTypes;

ThemeComponent.defaultProps = ThemeComponentDefaultProps;

import { string, func} from 'prop-types';

export const ThemeComponentPropTypes = {
    themeName: string,
    changeTheme: func
}

export const ThemeComponentDefaultProps = {
    themeName: "",
    changeTheme: (v)=>{}
}
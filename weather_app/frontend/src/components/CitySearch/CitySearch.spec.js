import React from 'react';
import { render, screen, fireEvent, waitFor, logRoles } from '@testing-library/react';
import { CitySearch } from './CitySearch';

const data = {
    defaultValue: "",
    cityList: [
        { name: "City A" },
        { name: "City B" }
    ],
}
it('Show menu list when click input', async () => {
    const onChange = jest.fn()
    render(<CitySearch defaultValue={data.defaultValue} cityList={data.cityList} onChange={onChange} />);
    const input = screen.getAllByPlaceholderText('textBoxCity.placeholder')
   
    fireEvent.click(input[0], new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
    }))
    expect(screen.getByText("City A")).toBeInTheDocument()
});

it('Select item and change value input', async () => {
    const onChange = jest.fn()
    render(<CitySearch cityList={data.cityList} onChange={onChange} />);
    const input = screen.getByPlaceholderText('textBoxCity.placeholder')
    fireEvent.click(input, new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
    }))

    const item = screen.getByText("City A")
    fireEvent.click(item, new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
    }))

    expect(input.value).toEqual("City A")
});

it('Fire change event when type', async () => {
    const onChange = jest.fn()
    render(<CitySearch cityList={data.cityList} onChange={onChange} />);
    const input = screen.getByPlaceholderText('textBoxCity.placeholder')
    fireEvent.click(input, new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
    }))

    fireEvent.change(input, { target: { value: '23' } })
    expect(onChange).toBeCalledTimes(1)
});
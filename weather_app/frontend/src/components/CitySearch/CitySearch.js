import TextField from '@mui/material/TextField';
import SearchIcon from '@mui/icons-material/Search';
import Paper from '@mui/material/Paper';
import MenuList from '@mui/material/MenuList';
import MenuItem from '@mui/material/MenuItem';
import ListItemText from '@mui/material/ListItemText';
import InputAdornment from '@mui/material/InputAdornment';
import React, { useState } from 'react';
import useTranslate from '../../hooks/translate';
import styled from 'styled-components';
import { CitySearchPropsType, CitySearchDefaultProps } from './CitySearch.propTypes'

const WrapperDropDownContent = styled(Paper)`
   z-index: 1;
   position: absolute;
   width: 100%;
   margin-top: 8px;
   max-height: 300px;
   overflow-y: auto;
`

const Container = styled.div`
    position: relative;
`

export const CitySearch = ({
    defaultValue,
    cityList,
    onChange,
    onSelect,
}) => {
    const { translate } = useTranslate();
    const [showDropdown, setShowDropDown] = useState(false);
    const [value, setValue] = useState(defaultValue);

    const onSelectItem = (item) => {
        setShowDropDown(false)
        onSelect(item);
        setValue(item?.name);
    }

    const onClick = () => {
        setShowDropDown(true)
    }

    return (
        <Container>
            <TextField
                onClick={onClick}
                fullWidth
                id="outlined-basic"
                label={translate('textBoxCity.label')}
                variant="outlined"
                placeholder={translate('textBoxCity.placeholder')}
                onChange={(evt) => {
                    setValue(evt.target.value)
                    onChange(evt)
                }}
                value={value}
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start">
                            <SearchIcon />
                        </InputAdornment>
                    ),
                }}
            />
            {
                showDropdown && Boolean(cityList.length) && (
                    <WrapperDropDownContent>
                        <Paper>
                            <MenuList >
                                {
                                    cityList.map((i,index) => {
                                        const { name = "" } = i;
                                        return (
                                            <MenuItem
                                                key={index}
                                                onClick={() => {
                                                    onSelectItem(i)
                                                }}
                                            >
                                                <ListItemText>{name}</ListItemText>
                                            </MenuItem>)
                                    })
                                }
                            </MenuList>
                        </Paper>
                    </WrapperDropDownContent>
                )
            }

        </Container>
    )
}

CitySearch.propTypes = CitySearchPropsType;

CitySearch.defaultProps = CitySearchDefaultProps;
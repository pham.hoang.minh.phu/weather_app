import { string, func, arrayOf, shape } from 'prop-types';

const cityType = shape({
    name : string
})

export const CitySearchPropsType = {
    defaultValue: string,
    cityList: arrayOf(cityType),
    onChange: func,
    onSelect: func,
}

export const CitySearchDefaultProps = {
    defaultValue: "",
    cityList: [],
    onChange: ()=>{},
    onSelect: ()=>{},
}
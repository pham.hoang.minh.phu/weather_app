import { useContext } from 'react';
import { LocaleComponent } from '../components/LocaleComponent/LocaleComponent';
import { ThemeComponent } from '../components/ThemeComponent/ThemeComponent';
import styled from 'styled-components';
import ThemeHandleContext from '../context/theme';
import useTranslate from '../hooks/translate';

const Container = styled.div`
    position: fixed;
    z-index: 10;
    display: flex;
    width: 100%;
    flex: 1;
    border-bottom: 1px solid gray;
    padding: 5px 0px;
    box-shadow: 5x 4px #888888;
    top: 4px;
    background-color: white;
`

const Wrapper = styled.div`
    flex: 1;
    display: flex;
    justify-content: flex-end;
`

const WrapperLeft = styled.div`
    justify-content: flex-start;
    align-items: center;
    flex: 1;
`


const WrapperRight = styled.div`
    justify-content: flex-end;
    align-items: center;
    display: flex;
    margin-right: 15px;
`

const WrapperComponent = styled.div`
    flex :1;
`

const WrapperLocale = styled(LocaleComponent)`
    flex:1;
`

const WrapperTheme = styled(ThemeComponent)`
    flex:1;
`

export const Header = () => {
    const { themeName, setThemeName } = useContext(ThemeHandleContext)
    const { language, languages, changeLanguage } = useTranslate();

    return (
        <Container>
            <Wrapper></Wrapper>
            <WrapperLeft></WrapperLeft>
            <WrapperRight>
                <WrapperComponent>
                    <WrapperLocale
                        language={language}
                        languages={languages}
                        changeLanguage={changeLanguage}
                    ></WrapperLocale>
                </WrapperComponent>
                <WrapperComponent>
                    <WrapperTheme
                        themeName={themeName}
                        changeTheme={setThemeName}
                    ></WrapperTheme>
                </WrapperComponent>
            </WrapperRight>
        </Container>
    )
}
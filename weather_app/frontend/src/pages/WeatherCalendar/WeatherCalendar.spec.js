import React from 'react';
import { render, screen, fireEvent, waitFor, logRoles } from '@testing-library/react';
import WeatherCalendar from './WeatherCalendar';
import ThemeHandler from '../../hoc/theme';

const data = {}

it('Render WeatherCalendar component', async () => {
    render(<ThemeHandler>
        <WeatherCalendar />
    </ThemeHandler>);
    expect(screen.getByTestId("test-weather-calendar")).toBeInTheDocument()
});

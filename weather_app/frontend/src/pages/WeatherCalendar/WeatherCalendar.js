import React, { useEffect, useState } from 'react';
import WeatherCard from '../../components/WeatherCard/WeatherCard';
import { CitySearch } from '../../components/CitySearch/CitySearch';
import styled from 'styled-components';
import Grid from '@mui/material/Grid';
import { useSelector, useDispatch } from 'react-redux';
import { actions } from '../../App.store'
import Container from '@mui/material/Container';
import debounce from 'lodash/debounce';
import CONFIG from '../../config/config';
import { WeatherCardSkeleton } from '../../components/Skeleton/Skeleton'

const DEFAULT_CITY = "London";
const DEFAULT_CITY_ID = 44418;

const ContainerStyle = styled(Container)`
    margin-top: 6px;
    margin-bottom: 6px;
`
const Wrapper = styled.div`
    flex: 1;
`

const DELAY_TIME_DEBOUNCE = 1000;

const WeatherCalendar = () => {
    const dispatch = useDispatch();
    const { cityList } = useSelector(state => state.city);
    const { weatherList, isFetching = [] } = useSelector(state => state.weather)
    const [isLoadDefault, setIsLoadDefault] = useState(false)

    const {
        cityActions,
        weatherActions
    } = actions;

    const onSelectedCity = (city) => {
        if (city.locationId) {
            dispatch(weatherActions.fetchWeatherByLocation({ locationId: city.locationId }))
        }
    }

    const debounceCitySearch = debounce((nextValue) => fetchCityList(nextValue), DELAY_TIME_DEBOUNCE);


    const fetchCityList = (textSearch) => {
        if (textSearch)
            dispatch(cityActions.fetchCityList({ nameSearch: textSearch }))
    }

    const onChangeCitySearch = (evt) => {
        if (evt?.target.value) {
            debounceCitySearch(evt?.target.value)
        }
    }

    const getWeatherImg = (weatherKey) => {
        return CONFIG.META_WEATHER_URL + `/weather/${weatherKey}.svg`
    }

    useEffect(()=>{
        if (!isLoadDefault){
            dispatch(weatherActions.fetchWeatherByLocation({ locationId: DEFAULT_CITY_ID }))
            setIsLoadDefault(true)
        }
        
    },[isLoadDefault])

    return (
        <ContainerStyle data-testid="test-weather-calendar">
            <Grid container spacing={2}>
                <Grid item container>
                    <Grid item xs={12} md={3}>
                        <CitySearch
                            defaultValue={DEFAULT_CITY}
                            cityList={cityList}
                            onSelect={onSelectedCity}
                            onChange={onChangeCitySearch}
                            width={100}></CitySearch>
                    </Grid>
                </Grid>
                {
                    isFetching && (
                        <>
                            <Grid item container>
                                <Grid item xs={12} md={12}>
                                    <Wrapper>
                                        <WeatherCardSkeleton></WeatherCardSkeleton>
                                    </Wrapper>
                                </Grid>
                            </Grid>
                            <Grid item container spacing={2}>
                                <Grid item xs={12} md={3}>
                                    <Wrapper>
                                        <WeatherCardSkeleton></WeatherCardSkeleton>
                                    </Wrapper>
                                </Grid>
                                <Grid item xs={12} md={3}>
                                    <Wrapper>
                                        <WeatherCardSkeleton></WeatherCardSkeleton>
                                    </Wrapper>
                                </Grid>
                                <Grid item xs={12} md={3}>
                                    <Wrapper>
                                        <WeatherCardSkeleton></WeatherCardSkeleton>
                                    </Wrapper>
                                </Grid>
                                <Grid item xs={12} md={3}>
                                    <Wrapper>
                                        <WeatherCardSkeleton></WeatherCardSkeleton>
                                    </Wrapper>
                                </Grid>
                            </Grid>
                        </>
                    )
                }
                {
                    !isFetching  &&  weatherList && weatherList[0] && (
                        <Grid item container>
                            <Grid item xs={12} md={12}>
                                <Wrapper>
                                    <WeatherCard
                                        date={weatherList[0].applicableDate}
                                        minTemp={weatherList[0].minTemp}
                                        maxTemp={weatherList[0].maxTemp}
                                        weatherImg={getWeatherImg(weatherList[0].weatherShortKey)}
                                        weatherShortKey={weatherList[0].weatherShortKey}
                                    />
                                </Wrapper>
                            </Grid>
                        </Grid>
                    )
                }
                {
                    !isFetching  && weatherList && (
                        <Grid item container spacing={2}>
                            {
                                weatherList.map((item, index) => {
                                    if (index > 0) {
                                        return <Grid item xs={12} md={3} key={index}>
                                            <Wrapper>
                                                <WeatherCard
                                                    date={item.applicableDate}
                                                    minTemp={item.minTemp}
                                                    maxTemp={item.maxTemp}
                                                    weatherImg={getWeatherImg(item.weatherShortKey)}
                                                    weatherShortKey={item.weatherShortKey}
                                                />
                                            </Wrapper>
                                        </Grid>
                                    }

                                    return null
                                })
                            }
                        </Grid>
                    )
                }
            </Grid>
        </ContainerStyle>
    )
}

export default WeatherCalendar;
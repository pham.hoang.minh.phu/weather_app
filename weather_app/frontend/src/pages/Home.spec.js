import React from 'react';
import { render, screen, fireEvent, waitFor, logRoles } from '@testing-library/react';
import { Home } from './Home';
import ThemeHandler from '../hoc/theme';

const data = {}

it('Render Home page', async () => {
    render(
    <ThemeHandler>
         <ThemeHandler>
            <Home />
        </ThemeHandler>
    </ThemeHandler>
   );
    expect(screen.getByTestId("test-homepage")).toBeInTheDocument()
});

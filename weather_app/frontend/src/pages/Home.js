import React, { Suspense } from 'react';
import styled from 'styled-components';
import WeatherCalendar from './WeatherCalendar/WeatherCalendar';
import { Footer } from '../layouts/Footer';
import { Header } from '../layouts/Header';

const Container = styled.div`
    display: block;
    flex:1;
    width:100%;
    height:100%;
`
const WeatherCalendarStyle = styled.div`
  width:100%;
  position:relative;
  top: 65px;
`

export const Home = ({
    isMobileView
}) => {
    return (
        <Container data-testid="test-homepage">
            <Suspense>
                <Header></Header>
                <WeatherCalendarStyle>
                    <WeatherCalendar isMobileView={isMobileView}></WeatherCalendar>
                </WeatherCalendarStyle>
                <Footer></Footer>
            </Suspense>
        </Container >
    )
}
import { all } from 'redux-saga/effects';
import { createCitySaga } from './city/city.saga';
import { createWeatherSaga } from './weather/weather.saga';

export default function createRootSaga() {
  return function* rootSaga() {
    yield all([
      createCitySaga(),
      createWeatherSaga(),
    ]);
  };
}

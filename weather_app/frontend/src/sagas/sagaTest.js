export const testListenedActions = (saga, expectedActions) => () => {
    const iterator = saga();
    const listenedActions = [];
  
    for (
      let yieldValue = iterator.next();
      !yieldValue.done;
      yieldValue = iterator.next()
    ) {
      const [action] = yieldValue.value.payload.args;
  
      listenedActions.push(action);
    }
  
    expectedActions.forEach(action => expect(listenedActions).toContain(action));
  };
  
  export const mockResponse = (
    body,
    status = 200,
    headers = { get: jest.fn() }
  ) => {
    return jest.spyOn(global, 'fetch').mockImplementation(() =>
      Promise.resolve({
        json: jest.fn().mockReturnValue(body),
        text: jest.fn().mockReturnValue(body),
        status,
        headers,
      })
    );
  };
  
import { types } from '../../redux/weather/weather.meta';
import { createWeatherSaga, fetchWeatherByLocation } from './weather.saga';
import { testListenedActions } from '../sagaTest';
import genericApiSaga from '../genericApiSaga';

describe('City side effects', () => {
  describe('listened actions', () => {
    const actions = [types.FETCH_WEATHER_BY_LOCATION];
    const testFunction = testListenedActions(createWeatherSaga, actions);

    it('should listen to correct action', testFunction);
  });

  describe('fetch weather by location', () => {
    const payload = { locationId: '1' };
    const iterator = fetchWeatherByLocation({ payload });
    let next;

    it('should call api get weather', () => {
      next = iterator.next();
      expect(next.value.payload.fn).toEqual(genericApiSaga);
    });

    it('should be done', () => {
      next = iterator.next();
      expect(next.done).toEqual(true);
    });
  });
});

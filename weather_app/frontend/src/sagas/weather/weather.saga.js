import {
    actionCreators as weatherActionCreators,
    types as weatherTypes,
} from '../../redux/weather/weather.meta';
import {
    takeLatest,
    call,
} from 'redux-saga/effects';
import { apiRequest } from '../../utils/apiRequest';
import genericApiSaga from '../genericApiSaga';
import { API_END_POINT } from '../../constants/endPoint';
import Config from '../../config/config';

function* fetchWeatherByLocation({ payload }) {
    const { locationId } = payload;
    if (locationId) {
        yield call(genericApiSaga, {
            completed: weatherActionCreators.fetchWeatherByLocationCompleted,
            gatewayCall: () =>
                apiRequest({
                    url: Config.REACT_APP_BASE_URL + API_END_POINT.WEATHER_BY_LOCATION.replace("{locationId}", locationId),
                    method: "GET",
                }),
            failed: weatherActionCreators.fetchWeatherByLocationFailed,
        })
    }

}

function* createWeatherSaga() {
    yield takeLatest(weatherTypes.FETCH_WEATHER_BY_LOCATION, fetchWeatherByLocation);
}

export { createWeatherSaga, fetchWeatherByLocation };
import { put, call } from 'redux-saga/effects';
import { actionCreators } from '../redux/city/city.meta';
import genericApiSaga from './genericApiSaga';

const params = {
    completed: actionCreators.fetchCityListCompleted,
    failed: actionCreators.fetchCityListFailed,
    gatewayCall: () => 'gatewayCall',
};

const assertEffectResult = (gen, response, action) => {
    // call api
    gen.next();
    // dispatch action
    const { value } = gen.next(response);
    expect(value).toStrictEqual(put(action));
    // done
    expect(gen.next().done).toBe(true);
};

describe('apiSaga suite', () => {
    let gen;

    beforeEach(() => {
        gen = genericApiSaga(params);
    });

    it('should call gatewayCall', () => {
        expect(gen.next().value).toEqual(call(params.gatewayCall));
    });

    it('should call gatewayCall with params if available', () => {
        expect(gen.next().value.payload.args).toEqual(
            call(params.gatewayCall).payload.args
        );
    });

    describe('with successful response code', () => {
        const assertSuccessfulResponse = responseCode => {
            assertEffectResult(
                gen,
                {
                    status: responseCode,
                    body: {},
                },
                actionCreators.fetchCityListCompleted()
            );
        };

        it('should put success action when 200', () => {
            assertSuccessfulResponse(200);
        });
    });

    describe('with failed response code', () => {
        const assertFailedResponse = responseCode => {
            assertEffectResult(
                gen,
                {
                    status: responseCode,
                    body: {},
                },
                actionCreators.fetchCityListFailed({ body: {}, status: responseCode })
            );
        };

        it('should put failed action if gatewayCall failed for status 404', () => {
            assertFailedResponse(404);
        });

        it('should put failed action if gatewayCall failed for status 500', () => {
            assertFailedResponse(500);
        });

        it('should put failed action if gatewayCall failed for status 501', () => {
            assertFailedResponse(501);
        });

        it('should put failed action if gatewayCall failed for status 504', () => {
            assertFailedResponse(504);
        });
    });

    describe('with before completed events', () => {
        const beforeCompletedFn = jest.fn();
        beforeEach(() => {
            gen = genericApiSaga({
                ...params,
                beforeCompleted: beforeCompletedFn,
            });
        });

        it('should invoke before action', () => {
            const response = {
                status: 200,
                data: {
                    id : "123213"
                },
            };
            // call api
            gen.next();

            // trigger tracking events
            gen.next(response);
            expect(beforeCompletedFn).toHaveBeenCalledWith(response.data);

            // dispatch action
            const { value } = gen.next();
            expect(value).toStrictEqual(
                put(actionCreators.fetchCityListCompleted(response.data))
            );

            // done
            expect(gen.next().done).toBe(true);
        });
    });
});

import { call, put } from 'redux-saga/effects';
import { HTTP_CODE } from '../constants/httpCode';

function* genericApiSaga({
    completed,
    failed,
    gatewayCall,
    beforeCompleted,
    onResponse,
}) {
    const started = Date.now();
    const response = yield call(gatewayCall);
    const payload = response.data

    if (onResponse) {
        yield onResponse(response);
    }

    switch (response.status) {
        case HTTP_CODE.NOT_FOUND:
            console.log(`[API] ${started} - failed after: ${Date.now() - started}ms`);
            if (failed) {
                yield put(failed(response));
            }
            return;
        case HTTP_CODE.TEMPORARILY_UNAVAILABLE:
        case HTTP_CODE.NOT_EXIST:
            console.log(
                `[API] ${started} - completed - no data after: ${Date.now() -
                started}ms`
            );
            if (failed) {
                yield put(failed(response));
            }
            break;
        case HTTP_CODE.OK:
            console.log(
                `[API] ${started} - completed after: ${Date.now() - started}ms`
            );
            if (beforeCompleted) {
                yield beforeCompleted(payload);
            }
            yield put(completed(payload));
            break;
        default:
            console.log(`[API] ${started} - failed after: ${Date.now() - started}ms`);
            if (failed) {
                yield put(failed(response));
            }
            return;
    }
}

export default genericApiSaga
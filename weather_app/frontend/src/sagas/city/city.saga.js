import {
    actionCreators as cityActionCreators,
    types as cityTypes,
} from '../../redux/city/city.meta';
import {
    takeLatest,
    call,
} from 'redux-saga/effects';
import { apiRequest } from '../../utils/apiRequest';
import genericApiSaga from '../genericApiSaga';
import { API_END_POINT } from '../../constants/endPoint';
import Config from '../../config/config';

function* fetchCityList({ payload }) {
    const { nameSearch } = payload;

    console.log("@@@ paylaod",payload)
    const params = {
        query: nameSearch
    }
    // if (nameSearch) {
        yield call(genericApiSaga,{
            completed: cityActionCreators.fetchCityListCompleted,
            gatewayCall: () =>
                apiRequest({
                    url: Config.REACT_APP_BASE_URL + API_END_POINT.SEARCH_CITY,
                    method: "GET",
                    params: params,
                }),
            failed: cityActionCreators.fetchCityListFailed
        });
    // }
}

function* createCitySaga() {
    yield takeLatest(cityTypes.FETCH_CITY_LIST, fetchCityList);
}

export { createCitySaga , fetchCityList };
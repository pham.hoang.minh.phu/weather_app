import { takeLatest } from 'redux-saga/effects';
import { types } from '../../redux/city/city.meta';
import { createCitySaga, fetchCityList } from './city.saga';
import { testListenedActions } from '../sagaTest';
import genericApiSaga from '../genericApiSaga';

describe('City side effects', () => {
  describe('listened actions', () => {
    const actions = [types.FETCH_CITY_LIST];
    const testFunction = testListenedActions(createCitySaga, actions);

    it('should listen to correct action', testFunction);
  });

  describe('fetch city list', () => {
    const payload = { nameSearch: 'New york' };
    const iterator = fetchCityList({ payload });
    let next;

    it('should search city', () => {
      next = iterator.next();
      expect(next.value.payload.fn).toEqual(genericApiSaga);
    });

    it('should be done', () => {
      next = iterator.next();
      expect(next.done).toEqual(true);
    });
  });
});

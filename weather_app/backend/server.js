const express = require("express");
const axios = require("axios");
const cors = require("cors");

const PORT = 3001;
const app = express();
app.listen(PORT);
app.use(cors())
app.get("/api/location/search", async (req, res, next) => {
    try{
        const { query } = req;

        console.log(query)
        const result = await axios.get(`https://www.metaweather.com/api/location/search?query=${query.query}`, {
            params: {
                ...query
            },
        });
        const { data } = result || {}
        res.send(data)
    }catch(ex){
        next(ex)
    }

})

app.get("/api/location/:locationId/", async (req, res,next) => {
    try{
        const { params, } = req;
        const result = await axios.get(`https://www.metaweather.com/api/location/${params.locationId}`);
        const { data } = result || {}
        res.send(data)
    }catch(ex){
        next(ex)
    }

})

app.use((err,req,res,next)=>{
    res.status(500).send(err)
})



console.log(`Server running port ${PORT}`)